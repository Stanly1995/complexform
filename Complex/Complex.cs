﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Complex
{
    class Complex
    {
        double _real;
        double _imaginary;
        public double real
        {
            get { return _real; }
        }
        public double imaginary
        {
            get { return _imaginary; }
        }

        public static Complex operator +(Complex a, Complex b)
        {
            return new Complex(a.real + b.real, a.imaginary + b.imaginary);
        }

        public static Complex operator +(Complex a, double b)
        {
            return new Complex(a.real + b, a.imaginary);
        }

        public static Complex operator +(double b, Complex a)
        {
            return new Complex(a.real + b, a.imaginary);
        }

        public static Complex operator -(Complex a, Complex b)
        {
            return new Complex(a.real - b.real, a.imaginary - b.imaginary);
        }

        public static Complex operator -(Complex a, double b)
        {
            return new Complex(a.real - b, a.imaginary);
        }

        public static Complex operator -(double b, Complex a)
        {
            return new Complex(a.real - b, a.imaginary);
        }

        public static Complex operator *(Complex a, Complex b)
        {
            return new Complex(a.real * b.real - a.imaginary * b.imaginary, a.real * b.imaginary + b.real * a.imaginary);
        }

        public static Complex operator *(Complex a, double b)
        {
            return new Complex(a.real * b, a.imaginary * b);
        }

        public static Complex operator *(double b, Complex a)
        {
            return new Complex(a.real * b, a.imaginary * b);
        }

        public static Complex operator /(Complex a, double b)
        {
            return new Complex(a.real / b, a.imaginary / b);
        }

        public static Complex operator /(double b, Complex a)
        {
            return new Complex(a.real / b, a.imaginary / b);
        }

        public static bool operator ==(Complex a, Complex b)
        {
            return (a.real == b.real && a.imaginary == b.imaginary);
        }

        public static bool operator !=(Complex a, Complex b)
        {
            return (a.real != b.real || a.imaginary != b.imaginary);
        }

        public Complex(double real, double imaginary)
        {
            _real = real;
            _imaginary = imaginary;
        }

        public override string ToString()
        {
            StringBuilder str = new StringBuilder();
            if (real != 0 && imaginary > 0)
                str.Append(real + "+" + imaginary + "i");
            else if (real != 0 && imaginary < 0)
                str.Append(real + "-" + imaginary + "i");
            else if (real != 0 && imaginary == 0)
                str.Append(real);
            else if (real == 0 && imaginary != 0)
                str.Append(imaginary + "i");
            else if (real == 0 && imaginary == 0)
                str.Append(0);
            str.Append("\r\n");
            return str.ToString();
        }
    }
}
