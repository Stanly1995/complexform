﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Complex
{
    public partial class MyForm : Form
    {
        public MyForm()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try {
                richTextBox1.Text = String.Empty;
                Complex complex1 = new Complex(Double.Parse(textBox1.Text), Double.Parse(textBox2.Text));
                Complex complex2 = new Complex(Double.Parse(textBox3.Text), Double.Parse(textBox4.Text));
                richTextBox1.AppendText("Число 1\r\n");
                richTextBox1.AppendText(complex1.ToString());
                richTextBox1.AppendText("Число 2\r\n");
                richTextBox1.AppendText(complex2.ToString());
                switch (comboBox1.Text)
                {
                    case "+":
                        richTextBox1.AppendText("Число 1 + Число 2\r\n");
                        richTextBox1.AppendText((complex1 + complex2).ToString());
                        break;
                    case "-":
                        richTextBox1.AppendText("Число 1 - Число 2\r\n");
                        richTextBox1.AppendText((complex1 - complex2).ToString());
                        break;
                    case "*":
                        richTextBox1.AppendText("Число 1 * Число 2\r\n");
                        richTextBox1.AppendText((complex1 * complex2).ToString());
                        break;
                    case "==":
                        richTextBox1.AppendText("Число 1 == Число 2\r\n");
                        richTextBox1.AppendText((complex1 == complex2).ToString());
                        break;
                    case "!=":
                        richTextBox1.AppendText("Число 1 != Число 2\r\n");
                        richTextBox1.AppendText((complex1 != complex2).ToString());
                        break;
                }
            }
            catch (FormatException)
            {
                richTextBox1.Text = "Неверно введены числа\r\n";
            }
        }

        private void MyForm_Load(object sender, EventArgs e)
        {
            richTextBox1.AppendText("Введите числа!");
        }

    }
}
